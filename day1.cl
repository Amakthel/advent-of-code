(defun fuel-needs (mass)
  (- (truncate (/ mass 3)) 2))
(defun recursive-needs (mass)
  (if (<= (fuel-needs mass) 0)
      0
      (+ (fuel-needs mass)
	 (recursive-needs (fuel-needs mass)))))
(defun fuel-counter (modules)
  (reduce #'+ (map 'list #'recursive-needs modules)))
