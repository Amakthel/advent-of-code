(defun dist (point)
  (+ (car point) (cdr point)))
;; a point is a location on the plane with respect to the origin, here
;; represented by a cons of its x and y coordinates.

;; a wire is a string or a list, consisting of a series of instructions, separated by
;; commas. Each instruction consists of a direction-- U D L or R--followed by a
;; number, indicating the length of the wire in that direction.
(defun wire->list (wire)
  (do* ((spacewire (substitute #\Space #\, wire))
	(str spacewire
	     (subseq str
		     (+ 1 (position #\Space str))))
	(dirlist (list (subseq str
			       0
			       (position #\Space str)))
		 (cons (subseq str
			       0
			       (position #\Space str))
		       dirlist)))
      ((not (position #\Space str)) (reverse dirlist))))

(defun instruction-converter (instruction)
  (let ((direction (subseq instruction 0 1))
	(distance (parse-integer (subseq instruction 1))))
    (defun extend-wire (partial-wire)
      (defun x-endpoint (wire-bit)
	(caar wire-bit))
      (defun y-endpoint (wire-bit)
	(cdar wire-bit))
      (do ((final-wire partial-wire
		       (cons (cond ((equal direction "D") (cons (x-endpoint final-wire)
								(- (y-endpoint final-wire)
								   1)))
				   ((equal direction "L") (cons (- (x-endpoint final-wire)
								   1)
								(y-endpoint final-wire)))
				   ((equal direction "U") (cons (x-endpoint final-wire)
								(+ (y-endpoint final-wire)
								   1)))
				   ((equal direction "R") (cons (+ (x-endpoint final-wire)
								   1)
								(y-endpoint final-wire))))
			     final-wire))
	   (wire-remaining distance
			   (- wire-remaining 1)))
	  ((<= wire-remaining 0) final-wire)))
    (lambda (partial-wire)
      (extend-wire partial-wire))))
;; a wirelist is a list of the points belonging to that wire. It is generated by
;; wire-processor.

(defun wirelist-generator (wire)
  (let ((dirlist (wire->list wire)))
    (do ((remaining-instructions dirlist
				 (cdr dirlist))
	 (wirelist (list (cons 0 0))
		   (funcall (instruction-converter (car remaining-instructions))
			    wirelist)))
	((not (cdr remaining-instructions))
	 (funcall (instruction-converter (car remaining-instructions))
		  wirelist)))))
(defun same-point (a b)
  (and (= (car a)
	  (car b))
       (= (cdr a)
	  (cdr b))))

(defun compare-wires (wire1 wire2)
  (let ((wone (wirelist-generator wire1))
	(wtwo (wirelist-generator wire2)))
    (remove-if (lambda (a)
		 (cond ((not a) t)
		       ((same-point '(0 . 0) a) t)
		       (t nil)))
	       (loop for i in wone
		  for n = (car (member-if (lambda (n)
					    (same-point i n))
					  wtwo))
		  collect n))))

(defun closest-crossing (wire1 wire2)
  (let* ((crossings (compare-wires wire1 wire2))
	 (distances (map 'list #'dist crossings))
	 (joined (mapcar #'cons crossings distances)))
    (defun closer (crs1 crs2)
      (<= (cdr crs1)
	  (cdr crs2)))
    (do ((crossings-left (cdr joined) (cdr crossings-left))
	 (closest (car joined) (if (closer closest
					   (car crossings-left))
				   closest
				   (car crossings-left))))
	((eq crossings-left nil) (car closest)))))
